import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { Server } from './infrastructure/server';
import { StudentService } from './services/student.service';
import { StudentCoursesComponent } from './student-courses/student-courses.component';
import { StudentInfoComponent } from './student-info/student-info.component';
import { StudentListComponent } from './student-list/student-list.component';

@NgModule({
  imports: [BrowserModule, FormsModule, ReactiveFormsModule],
  declarations: [AppComponent, StudentInfoComponent, StudentListComponent, StudentCoursesComponent],
  bootstrap: [AppComponent],
  providers: [StudentService, Server]
})
export class AppModule { }
