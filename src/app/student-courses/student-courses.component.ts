import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { forkJoin, Observable } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { Course, Student } from '../models';
import { StudentService } from '../services';

@Component({
  selector: 'app-student-courses',
  templateUrl: './student-courses.component.html',
  styleUrls: ['./student-courses.component.css']
})
export class StudentCoursesComponent implements OnChanges {
  @Input() student: Student;
  courseDetails: Course[];
  constructor(
    private studentService: StudentService,
  ) { }

  ngOnChanges(changes: SimpleChanges) {
    if (this.student) {
      this.updateStudentInfoFor(this.student);
    }
  }

  private updateStudentInfoFor(student: Student) {
    forkJoin(
      this.studentService.getCourseIdsForStudent(student.email)
    )
    this.studentService.getCourseIdsForStudent(student.email)
      .pipe(
        mergeMap((courseIds) => {
          const courseDetails$: Observable<Course>[] = [];
          courseIds.forEach(courseId => {
            courseDetails$.push(this.studentService.getCourse(courseId));
          });
          return forkJoin(...courseDetails$);
        })
      ).subscribe(
        (courseDetails: Course[]) => {
          this.courseDetails = courseDetails.sort((a, b) => a.name.localeCompare(b.name));
        },
        (err) => {
          console.log('Unknown error', err);
        }
      )
  }
}
