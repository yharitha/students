import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormControl } from '@angular/forms';

import { Student } from '../models';
import { StudentService } from '../services';


@Component({
  selector: 'app-student-list',
  templateUrl: './student-list.component.html',
  styleUrls: ['./student-list.component.css']
})
export class StudentListComponent implements OnInit {
  readonly STUDENT_SELECT_PLACEHOLDER = 'Select Student';
  students: Student[] = [];
  studentSelectControl: FormControl = new FormControl();
  @Output() onStudentSelect: EventEmitter<Student> = new EventEmitter<Student>();

  constructor(
    private studentService: StudentService,
  ) {
    this.studentSelectControl.valueChanges.subscribe(val => {
      this.onStudentSelect.emit(val)
    });
  }

  ngOnInit() {
    this.studentService.getStudents().subscribe(
      (students) => {
        this.students = students;
      },
      (err) => {
        console.log('Unknown error', err);
      }
    )
  }
}