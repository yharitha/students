import { Component } from '@angular/core';

import { Student } from './models';

@Component({
  selector: 'my-app',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  selectedStudent: Student = null;

  constructor() {
  }

  setSelectedStudent(student: Student) {
    this.selectedStudent = student;
  }
}
